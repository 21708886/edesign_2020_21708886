/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * Author			: Charl du Toit
  * Student number	: 21708886
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"
#include "ff.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include <sinewave.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct _Button
{
	bool b_is_down;
	bool b_is_tapped;
	uint32_t up_time;
	uint32_t down_time;
} Button;

enum eState
{
	Idle,
	Playback,
	Record,
	Select_Record
};

enum eButton
{
	BTN_One,
	BTN_Two,
	BTN_Three,
	BTN_Rec,
	BTN_Stop
};


/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TIMEOUT 100
#define STATUS_LENGTH 10
#define ZERO_ASCII 30
#define BOUNCE_DURATION 10
#define BUFFER_LENGTH 1024
#define SMOOTHING_FACTOR 0.125
#define NUM_CALLBACKS_20S 1722
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc2;
DMA_HandleTypeDef hdma_adc2;

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac1;

SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim8;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */
int32_t accumulator = 0;
int32_t average = 128;
int32_t num_samples = 0;
int32_t temp_sample = 0;
float smoothed_sample = 0;

uint16_t playback_buffer[BUFFER_LENGTH];
uint8_t record_buffer[BUFFER_LENGTH];
int8_t PCM_record_buffer[BUFFER_LENGTH];

enum eState state;
uint8_t current_slot = 0;
Button button_1 = {false, false, 0, 0};
Button button_2 = {false, false, 0, 0};
Button button_3 = {false, false, 0, 0};
Button button_rec = {false, false, 0, 0};
Button button_stop = {false, false, 0, 0};

FATFS fs;
FRESULT fres;
FIL sdfile;
bool b_savestart = false;
bool b_savemid = false;
bool b_loadmid = false;
bool b_loadstart = false;
UINT num;
int count = NUM_CALLBACKS_20S;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM6_Init(void);
static void MX_ADC2_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM8_Init(void);
/* USER CODE BEGIN PFP */

static void uart_transmit_record(uint8_t slot);
static void uart_transmit_play(uint8_t slot);
static void uart_transmit_stop();

static uint32_t button_read_pin(enum eButton button);
static void button_set_state(enum eButton button);

static void led_1_enable();
static void led_1_disable();
static void led_2_enable();
static void led_2_disable();
static void led_3_enable();
static void led_3_disable();
static void led_rec_enable();
static void led_rec_disable();
static void led_disable_all();

static void resolve_state_idle();
static void resolve_state_playback();
static void resolve_state_record();
static void resolve_state_select_record();

static void load_playback();


/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_DAC_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_USART2_UART_Init();
  MX_TIM4_Init();
  MX_TIM6_Init();
  MX_ADC2_Init();
  MX_SPI2_Init();
  MX_TIM8_Init();
  MX_FATFS_Init();
  /* USER CODE BEGIN 2 */

  uint8_t start_up[] = {127, 128, '2', '1', '7','0','8','8','8','6'};
  HAL_UART_Transmit(&huart2, start_up, STATUS_LENGTH, TIMEOUT);
  state = Idle;
  wave_init();
  HAL_TIM_Base_Start(&htim6);
  HAL_TIM_Base_Start(&htim8);

  fres = f_mount(&fs, "", 1);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	  button_set_state(BTN_One);
	  button_set_state(BTN_Two);
	  button_set_state(BTN_Three);
	  button_set_state(BTN_Rec);
	  button_set_state(BTN_Stop);

	  switch (state)
	  {
	   case Idle:
		   resolve_state_idle();
	   break;
	   case Playback:
	   	   resolve_state_playback();
	   break;
	   case Record:
	   	   resolve_state_record();
	   break;
	   case Select_Record:
	   	   resolve_state_select_record();
	   break;
	   default:
	   break;
	  }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC2_Init(void)
{

  /* USER CODE BEGIN ADC2_Init 0 */

  /* USER CODE END ADC2_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC2_Init 1 */

  /* USER CODE END ADC2_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc2.Instance = ADC2;
  hadc2.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc2.Init.Resolution = ADC_RESOLUTION_8B;
  hadc2.Init.ScanConvMode = DISABLE;
  hadc2.Init.ContinuousConvMode = DISABLE;
  hadc2.Init.DiscontinuousConvMode = DISABLE;
  hadc2.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
  hadc2.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T8_TRGO;
  hadc2.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc2.Init.NbrOfConversion = 1;
  hadc2.Init.DMAContinuousRequests = ENABLE;
  hadc2.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC2_Init 2 */

  /* USER CODE END ADC2_Init 2 */

}

/**
  * @brief DAC Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC_Init(void)
{

  /* USER CODE BEGIN DAC_Init 0 */

  /* USER CODE END DAC_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC_Init 1 */

  /* USER CODE END DAC_Init 1 */
  /** DAC Initialization 
  */
  hdac.Instance = DAC;
  if (HAL_DAC_Init(&hdac) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT1 config 
  */
  sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC_Init 2 */

  /* USER CODE END DAC_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi2.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_4;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */
  // internal clock 84MHz
  // prescalar = 83
  // new frequency = 1Mhz
  // auto reload = 500000
  // pulse = 250000
  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 83;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 500000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 250000;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */
  HAL_TIM_MspPostInit(&htim2);

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */
  // internal clock 84MHz
  // prescalar = 8399
  // new frequency = 10khz
  // auto reload = 5000
  // pulse = 2500
  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 8399;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 5000;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 2500;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */
//
  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */
//
  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 0;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 0;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */
//
  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */
  // internal clock 42MHz
    // prescalar = 951
  // update event @ 44.210 kHz
  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 951;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM8 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM8_Init(void)
{

  /* USER CODE BEGIN TIM8_Init 0 */
	// internal clock 42MHz
	 // prescalar = 18
	 // new frequency = 2.21Mhz
	 // auto reload = 50
	 // update event @ 44.210 kHz
  /* USER CODE END TIM8_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM8_Init 1 */

  /* USER CODE END TIM8_Init 1 */
  htim8.Instance = TIM8;
  htim8.Init.Prescaler = 18;
  htim8.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim8.Init.Period = 50;
  htim8.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim8.Init.RepetitionCounter = 0;
  htim8.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim8) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim8, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim8, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM8_Init 2 */

  /* USER CODE END TIM8_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 500000;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
  /* DMA1_Stream6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);
  /* DMA2_Stream2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD2_Pin|GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD2_Pin PA10 */
  GPIO_InitStruct.Pin = LD2_Pin|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA6 PA9 */
  GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PC7 */
  GPIO_InitStruct.Pin = GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : PB8 PB9 */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */


static void uart_transmit_record(uint8_t slot)
{
	uint8_t record[] = {127, 128, 'R', 'e', 'c','o','r','d','_', slot + ZERO_ASCII};
	HAL_UART_Transmit(&huart2, record, STATUS_LENGTH, TIMEOUT);
}

static void uart_transmit_play(uint8_t slot)
{
	uint8_t play[] = {127, 128, 'P', 'l', 'a','y','_','_','_', slot + ZERO_ASCII};
	HAL_UART_Transmit(&huart2, play, STATUS_LENGTH, TIMEOUT);
}

static void uart_transmit_stop()
{
	uint8_t stop[] = {127, 128, 'S', 't', 'o','p','_','_','_', '_'};
	HAL_UART_Transmit(&huart2, stop, STATUS_LENGTH, TIMEOUT);
}


static uint32_t button_read_pin(enum eButton button)
{
	uint32_t result = 0;
	switch (button)
	{
		case BTN_One:
			result = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8);
		break;
		case BTN_Two:
			result = HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_7);
		break;
		case BTN_Three:
			result = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_9);
		break;
		case BTN_Rec:
			result = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_6);
		break;
		case BTN_Stop:
			result = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9);
		break;
		default:
		break;
	}
	return result;
}

static void button_set_state(enum eButton button)
{
	Button * p_button;
	switch (button)
	{
		case BTN_One:
			p_button = &button_1;
		break;
		case BTN_Two:
			p_button = &button_2;
		break;
		case BTN_Three:
			p_button = &button_3;
		break;
		case BTN_Rec:
			p_button = &button_rec;
		break;
		case BTN_Stop:
			p_button = &button_stop;
		break;
		default:
		break;
	}

	p_button->b_is_tapped = false;
	uint32_t tick = HAL_GetTick();

	if (button_read_pin(button) )
	{
		// transition from up to down
		// HAL_Readpin = 1
		// Structure isDown = 0
		if (!p_button->b_is_down)
		{
			// bounce on release
			if ((tick - p_button->up_time) >= BOUNCE_DURATION)
			{
				p_button->b_is_down = true;
				p_button->down_time = tick;
			}
			//TEST METHOD:
			// Press button 1. If led 3 flashes then bounce on release has occurred
			// ENABLE NEXT LINE OF CODE
			//else led_3_enable();
		}
	}
	else
	{
		//transition from down to up
		// HAL_Readpin = 0
		// Structure isDown = 1
		if (p_button->b_is_down)
		{
			// bounce on press
			if ((tick - p_button->down_time) >= BOUNCE_DURATION )
			{
				p_button->up_time = tick;
				p_button->b_is_down = false;
				//successful transition
				p_button->b_is_tapped = true;
			}
			//TEST METHOD:
			// Press button 1. If led 2 flashes then bounce on press has occurred
			// ENABLE NEXT LINE OF CODE
			//else led_2_enable();
		}
	}
}

static void led_1_enable()
{
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
}

static void led_1_disable()
{
	HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
}

static void led_2_enable()
{
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
}

static void led_2_disable()
{
	HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
}

static void led_3_enable()
{
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
}

static void led_3_disable()
{
	HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_2);
}

static void led_rec_enable()
{
	 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 1);
}

static void led_rec_disable()
{
	 HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, 0);
}

static void led_disable_all()
{
	led_1_disable();
	led_2_disable();
	led_3_disable();
	led_rec_disable();
}

static void resolve_state_idle()
{
	bool b_ok = (state == Idle);
	if (button_1.b_is_tapped && b_ok /*&& slot1 has recording */)
	{
		fres = f_open(&sdfile, "record1.bin", FA_OPEN_EXISTING | FA_READ);
		if (fres == RES_OK)
		{
			state = Playback;
			b_ok = false;
			led_1_enable();
			current_slot = 1;
			uart_transmit_play(current_slot);
			b_loadstart = true;
			b_loadmid = true;
			load_playback();
			HAL_DAC_Stop_DMA(&hdac,DAC_CHANNEL_1 );
			HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)playback_buffer, BUFFER_LENGTH,  DAC_ALIGN_12B_R);
		}
	}
    if (button_2.b_is_tapped && b_ok /*&& slot2 has recording */)
	{
    	fres = f_open(&sdfile, "record2.bin", FA_OPEN_EXISTING | FA_READ);
    	if (fres == RES_OK)
    	{
			state = Playback;
			b_ok = false;
			led_2_enable();
			current_slot = 2;
			uart_transmit_play(current_slot);
			b_loadstart = true;
			b_loadmid = true;
			load_playback();
			HAL_DAC_Stop_DMA(&hdac,DAC_CHANNEL_1 );
			HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)playback_buffer, BUFFER_LENGTH,  DAC_ALIGN_12B_R);
    	}
	}
    if (button_3.b_is_tapped && b_ok /*&& slot3 has recording */)
	{
    	fres = f_open(&sdfile, "record3.bin", FA_OPEN_EXISTING | FA_READ);
    	if (fres == RES_OK)
    	{
			state = Playback;
			b_ok = false;
			led_3_enable();
			current_slot = 3;
			uart_transmit_play(current_slot);
			b_loadstart = true;
			b_loadmid = true;
			load_playback();
			HAL_DAC_Stop_DMA(&hdac,DAC_CHANNEL_1 );
			HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)playback_buffer, BUFFER_LENGTH,  DAC_ALIGN_12B_R);
    	}
	}
    if (button_rec.b_is_down && b_ok )
    {
        state = Select_Record;
        b_ok = false;
        led_disable_all();
    }
}

static void resolve_state_playback()
{
	bool b_ok = (state == Playback);
	load_playback();
	if (  (button_stop.b_is_tapped  || (fres != RES_OK) ) && b_ok)
	{
		state = Idle;
		b_ok = false;
		led_disable_all();
		uart_transmit_stop();
		current_slot = 0;
		HAL_DAC_Stop_DMA(&hdac,DAC_CHANNEL_1 );
		f_close(&sdfile);
	}

}

static void resolve_state_record()
{
	bool b_ok = (state == Record);
	if (b_savestart)
	{
		fres = f_write(&sdfile, PCM_record_buffer, BUFFER_LENGTH/2 ,&num);
		b_savestart = false;
		count--;
	}
	if (b_savemid)
	{
		fres = f_write(&sdfile, PCM_record_buffer + BUFFER_LENGTH/2, BUFFER_LENGTH/2 ,&num);
		b_savemid = false;
		count--;
	}
	if (  (button_stop.b_is_tapped  || count == 0  ) && b_ok)
	{
		state = Idle;
		b_ok = false;
		led_disable_all();
		uart_transmit_stop();
		current_slot = 0;
		HAL_ADC_Stop_DMA(&hadc2);
		f_close(&sdfile);
		count = NUM_CALLBACKS_20S;
	}
}

static void resolve_state_select_record()
{
	bool b_ok = (state == Select_Record);
	if (!button_rec.b_is_down && b_ok )
	{
		state = Idle;
		b_ok = false;
		led_disable_all();
	}
	if (button_1.b_is_tapped && b_ok )
	{
		state = Record;
		b_ok = false;
		led_1_enable();
		led_rec_enable();
		current_slot = 1;
		fres = f_open(&sdfile, "record1.bin", FA_CREATE_ALWAYS | FA_WRITE);
		uart_transmit_record(current_slot);

		HAL_ADC_Start_DMA(&hadc2, (uint32_t*)record_buffer, BUFFER_LENGTH);
	}
	if (button_2.b_is_tapped && b_ok )
	{
		state = Record;
		b_ok = false;
		led_2_enable();
		led_rec_enable();
		current_slot = 2;
		uart_transmit_record(current_slot);
		fres = f_open(&sdfile, "record2.bin", FA_CREATE_ALWAYS | FA_WRITE);
		HAL_ADC_Start_DMA(&hadc2, (uint32_t*)record_buffer, BUFFER_LENGTH);
	}
	if (button_3.b_is_tapped && b_ok )
	{
		state = Record;
		b_ok = false;
		led_3_enable();
		led_rec_enable();
		current_slot = 3;
		uart_transmit_record(current_slot);
		fres = f_open(&sdfile, "record3.bin", FA_CREATE_ALWAYS | FA_WRITE);
		HAL_ADC_Start_DMA(&hadc2, (uint32_t*)record_buffer, BUFFER_LENGTH);
		//record_3();
	}
}

//static void start_playback(uint8_t slot)
static void load_playback()
{
	if (b_loadstart)
	{
		fres = f_read(&sdfile, playback_buffer, BUFFER_LENGTH/2 ,&num);
		for ( int i = 0; i < BUFFER_LENGTH/2; i ++)
		{
			playback_buffer[i] = playback_buffer[i] + 2048;
		}
		b_loadstart = false;
	}
	if (b_loadmid)
		{
		fres = f_read(&sdfile, playback_buffer + BUFFER_LENGTH/2, BUFFER_LENGTH/2 ,&num);
		for ( int i = BUFFER_LENGTH/2; i < BUFFER_LENGTH; i ++)
		{
			playback_buffer[i] = playback_buffer[i] + 2048;
		}
		b_loadmid = false;
	}

//	if (slot > 0 && slot < 4)
//	{
//		HAL_DAC_Stop_DMA(&hdac,DAC_CHANNEL_1 );
//		wave_fillbuffer(playback_buffer, slot, BUFFER_LENGTH);
//		//populate and add 2048
//		HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)playback_buffer, BUFFER_LENGTH,  DAC_ALIGN_12B_R);
//	}
}
//static void continue_playback()
//{
//	if (loadmid == 1)
//	{
//		wave_fillbuffer(playback_buffer + BUFFER_LENGTH/2, current_slot, BUFFER_LENGTH/2);
//		loadmid = 0;
//	}
//	if (loadstart == 1)
//	{
//		wave_fillbuffer(playback_buffer, current_slot, BUFFER_LENGTH/2);
//		loadstart = 0;
//	}
//}

// DAC
void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	b_loadmid = true;
	HAL_DAC_Start_DMA(hdac, DAC_CHANNEL_1, (uint32_t *)playback_buffer, BUFFER_LENGTH,  DAC_ALIGN_12B_R);
	//wave_fillbuffer(playback_buffer + (BUFFER_LENGTH/2), current_slot, BUFFER_LENGTH/2 );
}


void HAL_DAC_ConvHalfCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	b_loadstart = true;
	//wave_fillbuffer(playback_buffer, current_slot, BUFFER_LENGTH/2 );
}

// ADC
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	huart2.gState = HAL_UART_STATE_READY;

	for (int i = BUFFER_LENGTH/2; i < BUFFER_LENGTH; i++)
	{
		accumulator += record_buffer[i];
		temp_sample = (int32_t)record_buffer[i] - average;
		smoothed_sample = SMOOTHING_FACTOR * temp_sample + (1 -SMOOTHING_FACTOR) * smoothed_sample;
		temp_sample = (int32_t)smoothed_sample;
		if (temp_sample > 127)
			temp_sample = 127;
		if (temp_sample < -128)
			temp_sample = -128;
		PCM_record_buffer[i] = (int8_t)temp_sample;
	}

	num_samples += BUFFER_LENGTH/2;
	if (num_samples >= BUFFER_LENGTH * 20)
	{
		average = accumulator/ num_samples;
		accumulator = 0;
		num_samples = 0;
	}
	b_savemid = true;
	HAL_UART_Transmit_DMA(&huart2, (uint8_t*)PCM_record_buffer + BUFFER_LENGTH/2 , BUFFER_LENGTH/2);
}

void HAL_ADC_ConvHalfCallback(ADC_HandleTypeDef* hadc)
{
	huart2.gState = HAL_UART_STATE_READY;

	for (int i = 0; i < BUFFER_LENGTH/2; i++)
	{
		accumulator += record_buffer[i];
		temp_sample = (int32_t)record_buffer[i] - average;
		smoothed_sample = SMOOTHING_FACTOR * temp_sample + (1 -SMOOTHING_FACTOR) * smoothed_sample;
		temp_sample = (int32_t)smoothed_sample;
		if (temp_sample > 127)
			temp_sample = 127;
		if (temp_sample < -128)
			temp_sample = -128;
		PCM_record_buffer[i] = (int8_t)temp_sample;
	}

	num_samples += BUFFER_LENGTH/2;
	if (num_samples >= BUFFER_LENGTH * 20)
	{
		average = accumulator/ num_samples;
		accumulator = 0;
		num_samples = 0;
	}
	b_savestart = true;
	HAL_UART_Transmit_DMA(&huart2, (uint8_t*)PCM_record_buffer , BUFFER_LENGTH/2);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
